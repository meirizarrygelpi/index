// Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí.
// Licensed under the MIT License.

package index

import "testing"

func TestLen(t *testing.T) {
	a := Array{0, 1, 2, 3}
	obv := a.Len()
	if obv != 4 {
		t.Fail()
	}
}

func TestClone(t *testing.T) {
	a := Array{0, 1, 2, 3}
	obv := a.Clone()
	if !obv.IsEqualTo(a) {
		t.Fail()
	}
}

func TestAntiCycle(t *testing.T) {
	a := Array{0, 1, 2}
	a.AntiCycle()
	obv := a.Clone()
	if !obv.IsEqualTo(Array{1, 2, 0}) {
		t.Fail()
	}
}

func TestCycle(t *testing.T) {
	a := Array{0, 1, 2}
	a.Cycle()
	obv := a.Clone()
	if !obv.IsEqualTo(Array{2, 0, 1}) {
		t.Fail()
	}
}
