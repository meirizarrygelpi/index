// Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí.
// Licensed under the MIT License.

package index

import (
	"strconv"
	"strings"

	"bitbucket.org/meirizarrygelpi/shuffle"
)

// Array type represents an array of distinct integers.
type Array []int

// Len method returns the length of an index array.
func (a Array) Len() int {
	return len(a)
}

// IsEqualTo method returns true if two index arrays have the same entries.
func (a Array) IsEqualTo(b Array) bool {
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

// String method returns the string representation of an index array.
func (a Array) String() string {
	s := make([]string, a.Len())
	for i, n := range a {
		s[i] = strconv.Itoa(n)
	}
	return strings.Join(s, "-")
}

// Clone method returns a copy of a given index array.
func (a Array) Clone() Array {
	b := make(Array, a.Len())
	copy(b, a)
	return b
}

// IdentityArray function returns an ordered index array of a given length.
func IdentityArray(n int) Array {
	a := make(Array, n)
	for i := range a {
		a[i] = i
	}
	return a
}

// AntiCycle method performs an in-place left cyclic shift on a given index
// array.
func (a Array) AntiCycle() {
	aFirst := a[0]
	copy(a, a[1:])
	a[len(a)-1] = aFirst
}

// Cycle method performs an in-place right cyclic shift on a given index array.
func (a Array) Cycle() {
	aLast := a[len(a)-1]
	copy(a[1:], a)
	a[0] = aLast
}

// Swap method performs an in-place swap of the ith and jth entry on a given
// index array.
func (a Array) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

// Reverse method performs an in-place mirror reflection on a given index
// array.
func (a Array) Reverse() {
	la := a.Len()
	for i := 0; i < la/2; i++ {
		a.Swap(i, la-i-1)
	}
}

// AllAntiCycles method returns a channel that receives all of the anticycles
// of a given index array.
func (a Array) AllAntiCycles() <-chan Array {
	out := make(chan Array)
	go func() {
		defer close(out)
		for i := 0; i < a.Len(); i++ {
			out <- a.Clone()
			a.AntiCycle()
		}
	}()
	return out
}

// AllCycles method returns a channel that receives all of the cycles
// of a given index array.
func (a Array) AllCycles() <-chan Array {
	out := make(chan Array)
	go func() {
		defer close(out)
		for i := 0; i < a.Len(); i++ {
			out <- a.Clone()
			a.Cycle()
		}
	}()
	return out
}

// CyclicInequivalent function returns a channel that receives all of the
// cyclic-inequivalent index arrays of a given length.
func CyclicInequivalent(k int) <-chan Array {
	out := make(chan Array)
	if k == 1 {
		go func() {
			defer close(out)
			out <- Array{0}
		}()
		return out
	}
	go func() {
		defer close(out)
		for a := range CyclicInequivalent(k - 1) {
			for b := range a.AllAntiCycles() {
				out <- append(b, k-1)
			}
		}
	}()
	return out
}

// ReflectionCyclicInequivalent function returns a channel that receives all of the
// mirror- and cyclic-inequivalent index arrays of a given length.
func ReflectionCyclicInequivalent(k int) <-chan Array {
	out := make(chan Array)
	if k == 1 {
		go func() {
			defer close(out)
			out <- Array{0}
		}()
		return out
	}
	if k == 2 {
		go func() {
			defer close(out)
			out <- Array{0, 1}
		}()
		return out
	}
	if k == 3 {
		go func() {
			defer close(out)
			out <- Array{0, 1, 2}
		}()
		return out
	}
	go func() {
		defer close(out)
		for a := range ReflectionCyclicInequivalent(k - 1) {
			for b := range a.AllAntiCycles() {
				out <- append(b, k-1)
			}
		}
	}()
	return out
}

// AllArrays function returns a channel receiving all index arrays of a given
// length.
func AllArrays(k int) <-chan Array {
	out := make(chan Array)
	go func() {
		defer close(out)
		for a := range CyclicInequivalent(k) {
			for b := range a.AllAntiCycles() {
				out <- b
			}
		}
	}()
	return out
}

// mergeAndCountSplitInv function merges two sorted index arrays and counts
// the number of split inversions.
func mergeAndCountSplitInv(a, b Array) (Array, int64) {
	var inv int64
	la, lb := a.Len(), b.Len()
	la64 := int64(la)
	n := la + lb
	c := make(Array, n)
	i, j := 0, 0
	for k := 0; k < n; k++ {
		if (i < la) && (j < lb) {
			if a[i] < b[j] {
				c[k] = a[i]
				i++
			} else {
				c[k] = b[j]
				j++
				inv = inv + la64 - int64(i)
			}
		} else if i > la-1 {
			c[k] = b[j]
			j++
		} else {
			c[k] = a[i]
			i++
		}
	}
	return c, inv
}

// sortAndCountInv function returns a sorted array and the number of
// inversions.
func sortAndCountInv(a Array) (Array, int64) {
	n := a.Len()
	if (n == 0) || (n == 1) {
		return a, 0
	}
	h := n / 2
	x, i := sortAndCountInv(a[:h])
	y, j := sortAndCountInv(a[h:])
	z, k := mergeAndCountSplitInv(x, y)
	return z, i + j + k
}

// Sort method returns a sorted index array.
func (a Array) Sort() Array {
	s, _ := sortAndCountInv(a)
	return s
}

// Inversions method returns the number of inversions in a given index array.
func (a Array) Inversions() int64 {
	_, inv := sortAndCountInv(a)
	return inv
}

// Sign method returns 1 if a given index array has an even number of
// inversions, -1 otherwise.
func (a Array) Sign() int {
	if (a.Inversions() % 2) == 0 {
		return 1
	}
	return -1
}

// AllEven function returns a channel receiving all of the index arrays of a
// given length with an even number of inversions.
func AllEven(k int) <-chan Array {
	out := make(chan Array)
	go func() {
		defer close(out)
		for p := range AllArrays(k) {
			if p.Sign() > 0 {
				out <- p
			}
		}
	}()
	return out
}

// AllOdd function returns a channel receiving all of the index arrays of a
// given length with an odd number of inversions.
func AllOdd(k int) <-chan Array {
	out := make(chan Array)
	go func() {
		defer close(out)
		for p := range AllArrays(k) {
			if p.Sign() < 0 {
				out <- p
			}
		}
	}()
	return out
}

// Shuffle method performs an in-place Fisher-Yates shuffle on a given index
// array.
func (a Array) Shuffle() {
	shuffle.FisherYates(a)
}
